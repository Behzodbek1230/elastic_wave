// Define your data
var x = [0, 1, 2, 3, 4];
var y = [0, 1, 4, 9, 16];

// Create a trace for your initial plot
var trace = {
  x: x,
  y: y,
  mode: "lines",
  type: "scatter",
};

// Create a list of frames for your animation
var frames = [];
for (var i = 0; i < 10; i++) {
  var yNew = y.map(function (val) {
    return val + i;
  });
  frames.push({
    data: [{ x: x, y: yNew }],
  });
}

// Define your layout
var layout = {
  title: "My Animation",
  xaxis: {
    range: [0, 4],
  },
  yaxis: {
    range: [0, 25],
  },
};

// Create your initial plot
Plotly.newPlot("bek", [trace], layout);

// Add the animation
Plotly.animate("bek", frames, { duration: 500, easing: "linear" });
