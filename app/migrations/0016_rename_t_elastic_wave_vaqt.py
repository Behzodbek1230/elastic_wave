# Generated by Django 4.1.3 on 2023-04-12 11:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0015_elastic_wave_aa_elastic_wave_bb'),
    ]

    operations = [
        migrations.RenameField(
            model_name='elastic_wave',
            old_name='T',
            new_name='Vaqt',
        ),
    ]
