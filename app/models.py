from django.db import models


class Elastic_Wave(models.Model):
    Vaqt=models.FloatField(max_length=50)
    lx=models.FloatField(max_length=50)
    ly=models.FloatField(max_length=50)

    # bo'laklar soni begin
    N=models.IntegerField()
    M=models.IntegerField()
    My=models.IntegerField()
    #bo'laklar soni end
    aa=models.JSONField(null=True)
    bb=models.JSONField(null=True)
    result=models.JSONField(null=True)
    def __int__(self):
        return self.Vaqt

class User(models.Model):
    name=models.CharField(max_length=20)
    age=models.IntegerField()
    def __str__(self):
        return self.name