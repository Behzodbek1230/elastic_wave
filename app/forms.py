from django import forms

class NameForm(forms.Form):
    Cs = forms.FloatField(label='Cs')

    Cp1 = forms.FloatField(label='Cp1')
    Cp2 = forms.FloatField(label='Cp2')
    pFol = forms.FloatField(label='pFol')
    pFos = forms.FloatField(label='pFos')
    d0 = forms.FloatField(label='d0')