import json
from django.shortcuts import render
import math
import numpy as np
import os
import pandas as pd
from django.http import JsonResponse
from django.core import serializers
from .models import Elastic_Wave,User
class NumpyEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def wiews(request):
    Z = []
    AA=[]
    BB=[]
    z1 = []
    z2 = []
    z1y=[]
    z2y=[]
    if request.method == "POST":
        # bo'laklar soni begin
        N = int(request.POST['N'])
        M = int(request.POST['M'])
        My = int(request.POST['My'])
        # bo'laklar soni end


        T = float(request.POST['T'])
        lx = int(request.POST['lx'])
        ly = int(request.POST['ly'])

        L1=15
        L2=25
        Ax1=40
        Bx1=55
        Ax2=5
        Bx2=30
        h1=(Bx1-Ax1)/lx
        h2=(Bx2-Ax2)/ly
        a=20*max(h1,h2)

        def Cp2(r):
            return 0.45
        def pFol(r):
            return 1000
        def pFos(r):
            if r*h2>=L1 and r*h2<=L2:
                return 1200
            else:
                return 1000
        def Cs(r):
            if r*h2>=L1 and r*h2<=L2:
                return 1300
            else:
                return 1400
        def Cp1(r):
              if r*h2>=L1 and r*h2<=L2:
                return 1400
              else:
                return 2000
        d0 = 0.2
        def fi1(t, x, y):
            return 0

        def fi2(t, x, y):
            return 0

        def fi3(t, x, y):
            return 0

        def fi4(t, x, y):
            return 0

        def fi5(t, x, y):
            return 0

        def fi6(t, x, y):
            return 0

        def fi7(t, x, y):
            return 0

        def fi8(t, x, y):
            return 0

        def p0s(r):
            return pFos(r) * (1 - d0)
        def p0l(r):
            return pFol(r) * d0
        def p0(r):
            return pFos(r) + pFol(r)
        def myu(r):
            return p0s(r)* (Cs(r)) ** 2
        f0 = 1400/(h1*My)
        t0=1/f0
        def K(r):
            return (p0(r) * p0s(r)) / (2 * p0l(r)) * ((Cp1(r))** 2 + (Cp2(r))** 2 - (8 / 3) * (p0l(r)/ p0(r)) * (Cs(r))** 2 -
                                      math.sqrt(
                                          ((Cp1(r))** 2 - (Cp2(r))** 2) ** 2 - (64 / 9) * (p0s(r)* p0l(r)/ p0(r) ** 2) * (Cs(r))** 4))
        gamma = 4
        def alpha3(r):
            return  1 / (2 * p0(r) ** 2) * ((Cp1(r))** 2 + (Cp2(r))** 2 - (8 / 3) * (p0s(r)/ p0(r)) * (Cs(r))** 2 +
                                      math.sqrt(
                                          ((Cp1(r))** 2 - (Cp2(r))** 2) ** 2 - (64 / 9) * (p0s(r)* p0l(r)/ p0(r) ** 2) * (Cs(r))** 4))

        def alpha(r):
            return p0(r) * alpha3(r) + K(r) / p0(r) ** 2
        def betta1(r):
             if r>=L1 and r<=L2:
                return  p0s(r)* K(r) / p0(r) - 2 * myu(r) / 3
             else:
                return p0(r)*(Cp1(r)**2-2*Cs(r)**2)
        def betta2(r):
             if r>=L1 and r<=L2:
                return  p0s(r)* K(r) / p0(r)
             else:
                return 0
        def betta3(r):
             if r>=L1 and r<=L2:
                return  K(r)-alpha(r)*p0(r)*pFos(r)
             else:
                return -p0(r)*(Cp1(r)**2-(4/3)*Cs(r)**2)
        # r ga qiymat berish
        x0 = 50
        y0 = 14
        e = 2.71
        def delta(x, y, x0, y0, a):
            if math.sqrt((x - x0) ** 2 + (y - y0) ** 2) < a:
                return e ** (-a ** 2 / (a ** 2 - (x - x0) ** 2 - (y - y0) ** 2))
            else:
                return 0
        def deltax(x, y, x0, y0, a):
            if math.sqrt((x - x0) ** 2 + (y - y0) ** 2) < a:
                return (-2 * a ** 2 * (x - x0) / (a ** 2 - (x - x0) ** 2 - (y - y0) ** 2) ** 2) * e ** (
                        -a ** 2 / (a ** 2 - (x - x0) ** 2 - (y - y0) ** 2))
            else:
                return 0
        def deltay(x, y, x0, y0, a):
            if math.sqrt((x - x0) ** 2 + (y - y0) ** 2) < a:
                return (-2 * a ** 2 * (y - y0) / (a ** 2 - (x - x0) ** 2 - (y - y0) ** 2) ** 2) * e ** (
                        -a ** 2 / (a ** 2 - (x - x0) ** 2 - (y - y0) ** 2))
            else:
                return 0
        def f(t):
            return math.sin(2 * math.pi * f0 * (t - t0)) * e ** (-(2 * math.pi * f0 * (t - t0)) ** 2) / gamma ** 2
        def F1(t, x, y):
            return f(t) * deltax(x - 2 * t, y, x0, y0, a) * delta(x, y, x0, y0, a)
        def F2(t, x, y):
            return f(t) * deltay(x - 2 * t, y, x0, y0, a) * delta(x, y, x0, y0, a)
        x = []
        y = []
        t = []
        h = float(lx / M)
        hy = float(ly / My)
        tao = float(9*10**(-5)*h1/(20*math.sqrt(10)))
        x = np.arange(M + 1, dtype='f').reshape(M + 1)
        y = np.arange(My + 1, dtype='f').reshape(My + 1)
        t = np.arange(N + 1, dtype='f').reshape(N + 1)
        for i in range(0, M + 1):
            x[i] = Ax1 + i * h1
        for n in range(0, N + 1):
            t[n] = (tao * n)
        for j in range(0, My + 1):
            y[j] = (Ax2 + j * h2)
        w1 = np.zeros((M + 1, My + 1))
        w2 = np.zeros((M + 1, My + 1))
        w3 = np.zeros((M + 1, My + 1))
        w4 = np.zeros((M + 1, My + 1))
        w5 = np.zeros((M + 1, My + 1))
        w6 = np.zeros((M + 1, My + 1))
        w7 = np.zeros((M + 1, My + 1))
        w8 = np.zeros((M + 1, My + 1))
        S = []
        def E():
            for i in range(0, M + 1):
                for j in range(0, My + 1):
                    w1[i][j] = fi1(0, x[i], y[j])
                    w2[i][j] = fi2(0, x[i], y[j])
                    w3[i][j] = fi3(0, x[i], y[j])
                    w4[i][j] = fi4(0, x[i], y[j])
                    w5[i][j] = fi5(0, x[i], y[j])
                    w6[i][j] = fi6(0, x[i], y[j])
                    w7[i][j] = fi7(0, x[i], y[j])
                    w8[i][j] = fi8(0, x[i], y[j])
            Array = [w1, w2, w3, w4, w5, w6, w7, w8]
            S.append(np.array(Array))
            for n in range(0, N):
                for i in range(0, M):
                    for j in range(1, My):
                        w1[i][j] = w1[i][j] - (tao / pFos(j)) * ((w4[i+1][j]-w4[i][j])/h1-(w5[i][j+1]-w5[i][j])/h2)-(tao/p0(j))*(w8[i+1][j]-w8[i][j])/h1+tao*F1(t[n],x[i],y[j])
                        w2[i][j] = w2[i][j] - (tao / p0s(j)) * ((w6[i+1][j]-w6[i][j])/h1-(w7[i][j+1]-w7[i][j])/h2)-(tao/p0(j))*(w8[i+1][j]-w8[i][j])/h2+tao*F2(t[n],x[i],y[j])
                        w3[i][j] = w3[i][j] - (tao / p0(j)) *(w8[i+1][j] - w8[i][j])/h1+tao*F1(t[n],x[i],y[j])
                        w4[i][j] = w4[i][j] - (tao / p0(j)) *(w8[i][j+1] - w8[i][j])/h2+tao*F2(t[n],x[i],y[j])
                        w5[i][j]=w5[i][j]-2*myu(j)*(tao/h1)*(w1[i+1][j]-w1[i][j])+(tao/h1)*betta1(j)*(w1[i+1][j]-w1[i][j])+(tao/h2)*betta1(j)*(w2[i][j+1]-w2[i][j])+(tao/h1)*betta2(j)*(w3[i+1][j]-w3[i][j])+(tao/h2)*betta2(j)*(w3[i][j+1]-w3[i][j])
                        w6[i][j] = w6[i][j] - myu(j)*(tao / h2) *(w1[i][j+1] - w1[i][j])- myu(j)*(tao / h1) *(w2[i+1][j] - w2[i][j])
                        w7[i][j]=w7[i][j]-2*myu(j)*(tao/h2)*(w2[i][j+1]-w2[i][j])-(tao/h1)*betta1(j)*(w1[i+1][j]-w1[i][j])-(tao/h2)*betta1(j)*(w2[i][j+1]-w2[i][j])+(tao/h1)*betta2(j)*(w3[i+1][j]-w3[i][j])+(tao/h2)*betta2(j)*(w3[i][j+1]-w3[i][j])
                        w8[i][j]=w8[i][j]+betta3(j)*(tao/h1)*(w1[i+1][j]-w1[i][j])+(tao/h2)*betta3(j)*(w2[i][j+1]-w2[i][j])-(tao/h1)*alpha(j)*p0(j)*p0l(j)*(w3[i+1][j]-w3[i][j])-(tao/h2)*alpha(j)*p0(j)*p0l(j)*(w3[i][j+1]-w3[i][j])
                        w1[i][My]=0
                        w2[i][My]=0
                        w3[i][My]=0
                        w4[i][My]=0
                        w5[i][My]=0
                        w6[i][My]=0
                        w7[i][My]=0
                        w8[i][My]=0
                    w1[M][j] = 0
                    w2[M][j] = 0
                    w3[M][j] = 0
                    w4[M][j] = 0
                    w5[M][j] = 0
                    w6[M][j] = 0
                    w7[M][j] = 0
                    w8[M][j] = 0
                Array = [w1, w2, w3, w4, w5, w6, w7, w8]
                S.append(np.array(Array))
            return S
        aa= np.zeros((M + 1, My + 1))
        bb = np.zeros((M + 1, My + 1))
        # lists = E()
        def E2(E,k1):
            w1=np.array(E[k1][0])
            w2=np.array(E[k1][1])
            w3=np.array(E[k1][2])
            w4=np.array(E[k1][3])
            for i in range(0, M + 1):
                for j in range(0, My + 1):
                    bb[i][j]=math.sqrt(w3[i][j]**2 +w4[i][j]**2)
                    aa[i][j]=math.sqrt(w1[i][j]**2 +w2[i][j]**2)
            return [aa,bb]
        # Z.append(json.dumps(lists, cls=NumpyEncoder))
        for n in range(N+1):
            zx=[]
            for i in range(M+1):
                zy=[]
                for j in range(My+1):
                    zy.append(fi1(t[n],x[i],y[j]))
                zx.append(zy)
            z1.append(zx)
        for n in range(N + 1):
            zxy = []
            for j in range(My + 1):
                zyy = []
                for i in range(M + 1):
                    zyy.append(fi1(t[n], x[i], y[j]))
                zxy.append(zy)
            z1y.append(zxy)
    else:
        return render(request, 'base.html')
    let=(Elastic_Wave.objects.filter(Vaqt__lte= T))
    json_data = serializers.serialize('json', let)

    resarr=[]
    print(len(let))
    elastic_wave_list_json=[d['fields'] for d in json.loads(json_data)]
    for i in elastic_wave_list_json:
        resarr.append(json.loads(i['aa']))
    data = list(Elastic_Wave.objects.values())
    if (data):
        print(resarr)



        # Elastic_Wave.objects.create(N=N,M=M,My=My,T=T,lx=lx,ly=ly,aa=json.dumps(E2(lists,10)[0], cls=NumpyEncoder),
        # bb=json.dumps(E2(lists,10)[1], cls=NumpyEncoder),result=Z[0])


        context = {
            "M": M, "N": N, "T": T, "My": My, "lx": lx, "ly": ly,
            "data": data[4]['result'],
            'datajson':(resarr),
            # "x":x,
            # "y":y,
            "z1": z1,
            "z2": z1y,
            "tao":tao,
            "h":h,
            "hy":hy,
            "aa":data[4]['aa'],
            "bb": data[4]['bb']

        }
    else:
        context = {
            "M": M, "N": N, "T": T, "My": My, "lx": lx, "ly": ly,
            "data": Z,
            "z1": z1,
            "z2": z1y
        }

    return render(request, 'base.html', context)


